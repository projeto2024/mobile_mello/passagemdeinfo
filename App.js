import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import TaskDetails from "./src/taskDetails";
import TaskList from "./src/taskList";

const stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <stack.Navigator initialRouteName="ListaDeTarefas">
        <stack.Screen name="ListaDeTarefas" component={TaskList}/>
        <stack.Screen 
        name="DetalheDasTarefas" 
        component={TaskDetails}
        options={{title:"Informação das Tarefas"}}
        />
      </stack.Navigator>
    </NavigationContainer>
  );
};