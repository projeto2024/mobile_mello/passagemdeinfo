import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
    const tasks= [
        {
            id: 1,
            title: 'Ir ao Mercado',
            date: '2024-02-27',
            time: '10:00',
            address: 'Super SS'
        },
        {
            id: 2,
            title: 'Fazer exercícios',
            date: '2024-02-28',
            time: '08:00',
            address: 'Academia'
        },
        {
            id: 3,
            title: 'Ler um livro',
            date: '2024-02-29',
            time: '15:00',
            address: 'Em casa'
        }
    ];

    const taskPress=(task)=>{
        navigation.navigate('DetalheDasTarefas',{task})
    }

    return (
        <View>
            <FlatList
                data={tasks}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => taskPress(item)}>
                        <Text>{item.title}</Text>
                    </TouchableOpacity>
                )}
            />
        </View>
    );
};

export default TaskList;
